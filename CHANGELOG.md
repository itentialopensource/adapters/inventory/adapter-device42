
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:36PM

See merge request itentialopensource/adapters/adapter-device42!12

---

## 0.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-device42!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:51PM

See merge request itentialopensource/adapters/adapter-device42!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:04PM

See merge request itentialopensource/adapters/adapter-device42!8

---

## 0.4.0 [05-10-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/inventory/adapter-device42!7

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:46PM

See merge request itentialopensource/adapters/inventory/adapter-device42!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:03PM

See merge request itentialopensource/adapters/inventory/adapter-device42!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:12PM

See merge request itentialopensource/adapters/inventory/adapter-device42!4

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:40PM

See merge request itentialopensource/adapters/inventory/adapter-device42!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-device42!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-device42!1

---

## 0.1.1 [03-19-2021]

- Initial Commit

See commit 8169779

---
