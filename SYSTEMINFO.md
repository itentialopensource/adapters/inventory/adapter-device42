# Device42

Vendor: Device42
Homepage: https://www.device42.com/

Product: Device42
Product Page: https://www.device42.com/

## Introduction
We classify Device42 into the Inventory domain as Device42 is used for managing assets including hardware devices, software applications and cloud resources. We also classify it into the Network Services domain because it provides capabilities in IPAM, network integration and configuration automation.

"Device42 is an asset management solution that enables IT administrators to track hardware, software, devices and networks"

## Why Integrate
The Device42 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Device42.

With this adapter you have the ability to perform operations with Device42 such as:

- Manage IP address assignemnts, reservations and allocations for devices
- Create and manage IP subnets, VLANs

## Additional Product Documentation
The [API documents for Device42](https://api.device42.com/)