# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Device42 System. The API that was used to build the adapter for Device42 is usually available in the report directory of this adapter. The adapter utilizes the Device42 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Device42 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Device42.

With this adapter you have the ability to perform operations with Device42 such as:

- Manage IP address assignemnts, reservations and allocations for devices
- Create and manage IP subnets, VLANs

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
